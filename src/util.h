#ifndef	UTIL_H_INCLUDED
#define	UTIL_H_INCLUDED

#include	<stdlib.h>
#include	<iostream>
#include	<sstream>
#include	<fstream>
#include	<cmath>
#include	<limits>       // std::numeric_limits

/**
  Converts a string to an arbitrary type. >> operator must be defined for the target type.
  @param string string which should be converted
  @return converted string
  **/
template<typename T>
T StringTo(const std::string& string) {
    T valor;

    std::stringstream stream(string);
    stream >> valor;
    return valor;
}


#endif
