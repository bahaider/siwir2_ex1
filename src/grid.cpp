#include "grid.h"

#include <iostream>
#include <fstream>

Grid::Grid(const int numberOfPointsRS) :
	mNumberOfPointsRS(numberOfPointsRS){

	mh = 1.0 / (mNumberOfPointsRS - 1);
	mh2 = mh * mh;
	minvh2 = 1.0 / mh2;

	mld = (mNumberOfPointsRS + 1) / 2 + 13;

	uBlack = std::unique_ptr<double[]>(new double[mld * mNumberOfPointsRS]);
	uRed = std::unique_ptr<double[]>(new double[mld * mNumberOfPointsRS]);

	fBlack = std::unique_ptr<double[]>(new double[mld * mNumberOfPointsRS]);
	fRed = std::unique_ptr<double[]>(new double[mld * mNumberOfPointsRS]);

	rBlack = std::unique_ptr<double[]>(new double[mld * mNumberOfPointsRS]);
	rRed = std::unique_ptr<double[]>(new double[mld * mNumberOfPointsRS]);

	memset(uBlack.get(), 0, sizeof(double) * mld * mNumberOfPointsRS);
	memset(uRed.get(), 0, sizeof(double) * mld * mNumberOfPointsRS);
	memset(fBlack.get(), 0, sizeof(double) * mld * mNumberOfPointsRS);
	memset(fRed.get(), 0, sizeof(double) * mld * mNumberOfPointsRS);
	memset(rBlack.get(), 0, sizeof(double) * mld * mNumberOfPointsRS);
	memset(rRed.get(), 0, sizeof(double) * mld * mNumberOfPointsRS);
}

double& Grid::operator()(const int r, const int s){
	return getU(r, s);
}

double& Grid::getU(const int r, const int s){
	if (s % 2 == 0) {
		if (r % 2 == 0) {
			return getuRed(r / 2, s);
		}
		else {
			return getuBlack(r / 2, s);
		}
	}
	else {
		if (r % 2 == 1) {
			return getuRed(r / 2, s);
		}
		else {
			return getuBlack(r / 2, s);
		}
	}
}

double& Grid::getF(const int r, const int s){
	if (s % 2 == 0) {
		if (r % 2 == 0) {
			return getfRed(r / 2, s);
		}
		else {
			return getfBlack(r / 2, s);
		}
	}
	else {
		if (r % 2 == 1) {
			return getfRed(r / 2, s);
		}
		else {
			return getfBlack(r / 2, s);
		}
	}
}

double& Grid::getR(const int r, const int s){
	if (s % 2 == 0) {
		if (r % 2 == 0) {
			return getrRed(r / 2, s);
		}
		else {
			return getrBlack(r / 2, s);
		}
	}
	else {
		if (r % 2 == 1) {
			return getrRed(r / 2, s);
		}
		else {
			return getrBlack(r / 2, s);
		}
	}
}

void Grid::saveToFile(const std::string& filename){
	std::ofstream	fOut(filename);
	for (int s = 0; s < getNumberOfPointsS(); ++s) {
		for (int r = 0; r < getNumberOfPointsR(); ++r) {
			fOut << getX(r, s) << "\t" << getY(r, s) << "\t" << operator()(r, s) << std::endl;
		}
		fOut << std::endl;
	}
	fOut.close();
}
