#include "multigrid.h"

Multigrid::Multigrid(const int levels){
	gridLevels.reserve(levels);
	for (int i = levels; i>0; --i){
		gridLevels.push_back(std::unique_ptr<Grid> (new Grid( std::lround(pow(2, i)) + 1)));
	}
	initFinestGrid();
}

Multigrid::~Multigrid(){
	gridLevels[0]->saveToFile("test.dat");
	gridLevels[0]->saveToFile("solution.txt");
}

void Multigrid::initFinestGrid(){
	gridLevels[0]->setNorthBorder(function::g);
	gridLevels[0]->setSouthBorder(function::g);
	gridLevels[0]->setWestBorder(function::g);
	gridLevels[0]->setEastBorder(function::g);
}
