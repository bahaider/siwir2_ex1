#ifndef GRID_H_INCLUDED
#define	GRID_H_INCLUDED

#include <cstring>

#include <memory>
#include <string>

/**
Storage class for u and f (Au = f)

Implements red black storing scheme in x major order!
Domain is of size (0, 1) x (0, 1).
Coordinates in real space are denoted by x and y.
Coordinates in total grid space are denoted by r and s.
Coordinates in red black grids are denoted by a and b.
**/
class Grid {
private:
	///number of points in x and y direction including both borders
	int		mNumberOfPointsRS;
	///spacing between two points
    double	mh;
	double	mh2;
	double  minvh2;

	///size of one memory line
	int		mld;

	//red black storage for Au = f
	std::unique_ptr<double[]> uBlack;
	std::unique_ptr<double[]> uRed;
	std::unique_ptr<double[]> fBlack;
	std::unique_ptr<double[]> fRed;
	std::unique_ptr<double[]> rBlack;
	std::unique_ptr<double[]> rRed;

	///returns linearized index of a 2d coordinate
	inline
	int getLinIndex(const int r, const int s) const {
		return s * mld + r;
	}
public:
	Grid(const int numberOfPointsRS);

	//gets u
	double& operator()(const int r, const int s);

	//gets u
	double& getU(const int r, const int s);
	//gets f
	double& getF(const int r, const int s);
	//gets r
	double& getR(const int r, const int s);

	inline
	double geth() const;
	inline
	double geth2() const;
	inline
	double getinvh2() const;
	
	//Return number of points in each direction
	inline
		int getNumberOfPointsR() const;
	inline
		int getNumberOfPointsS() const;
	
	//Convert from grid to real space
	inline
		double getX(const int r, const int s) const;
	inline
		double getY(const int r, const int s) const;

	//Convert from red black grid to total grid
	inline
		int getRFromRed(const int a, const int b) const;
	inline
		int getSFromRed(const int a, const int b) const;
	inline
		int getRFromBlack(const int a, const int b) const;
	inline
		int getSFromBlack(const int a, const int b) const;

	inline
		double& getuRed(const int a, const int b);
	inline
		double& getuBlack(const int a, const int b);

	inline
		double& getfRed(const int a, const int b);
	inline
		double& getfBlack(const int a, const int b);

	inline
		double& getrRed(const int a, const int b);
	inline
		double& getrBlack(const int a, const int b);

	template <typename T>
	void setGrid(T func);

	template <typename T>
	void setInterior(T func);

	template <typename T>
	void setNorthBorder(T func);
	template <typename T>
	void setSouthBorder(T func);
	template <typename T>
	void setWestBorder(T func);
	template <typename T>
	void setEastBorder(T func);
	
	///Save grid content to file (gnuplot compatible)
	void saveToFile(const std::string& filename);
};

inline
double Grid::geth() const{
	return mh;
}

inline
double Grid::geth2() const{
	return mh2;
}

inline
double Grid::getinvh2() const{
	return minvh2;
}

inline
int Grid::getNumberOfPointsR() const {
	return mNumberOfPointsRS;
}
inline
int Grid::getNumberOfPointsS() const {
	return mNumberOfPointsRS;
}

inline
double Grid::getX(const int r, const int s) const {
	(void)s;
	return r * mh;
}
inline
double Grid::getY(const int r, const int s) const {
	(void)r;
	return s * mh;
}

inline
int Grid::getRFromRed(const int a, const int b) const {
	return a * 2 + b % 2;
}
inline
int Grid::getSFromRed(const int a, const int b) const {
	(void)a;
	return b;
}

inline
int Grid::getRFromBlack(const int a, const int b) const {
	return a * 2 + (b + 1) % 2;
}
inline
int Grid::getSFromBlack(const int a, const int b) const {
	(void)a;
	return b;
}

inline
double& Grid::getuRed(const int a, const int b){
	return uRed[getLinIndex(a, b)];
}
inline
double& Grid::getuBlack(const int a, const int b){
	return uBlack[getLinIndex(a, b)];
}

inline
double& Grid::getfRed(const int a, const int b){
	return fRed[getLinIndex(a, b)];
}
inline
double& Grid::getfBlack(const int a, const int b){
	return fBlack[getLinIndex(a, b)];
}

inline
double& Grid::getrRed(const int a, const int b){
	return rRed[getLinIndex(a, b)];
}
inline
double& Grid::getrBlack(const int a, const int b){
	return rBlack[getLinIndex(a, b)];
}

template <typename T>
void Grid::setGrid(T func){
	for (int s = 0; s < getNumberOfPointsS(); ++s) {
		for (int r = 0; r < getNumberOfPointsR(); ++r) {
			operator()(r, s) = func(getX(r, s), getY(r, s));
		}
	}
}

template <typename T>
void Grid::setInterior(T func){
	for (int s = 1; s < getNumberOfPointsS() - 1; ++s) {
		for (int r = 1; r < getNumberOfPointsR() - 1; ++r) {
			operator()(r, s) = func(getX(r, s), getY(r, s));
		}
	}
}

template <typename T>
void Grid::setNorthBorder(T func){
	int s = getNumberOfPointsS() - 1;
	for (int r = 1; r < getNumberOfPointsR() - 1; ++r) {
		operator()(r, s) = func(getX(r, s), getY(r, s));
	}
}

template <typename T>
void Grid::setSouthBorder(T func){
	int s = 0;
	for (int r = 1; r < getNumberOfPointsR() - 1; ++r) {
		operator()(r, s) = func(getX(r, s), getY(r, s));
	}
}

template <typename T>
void Grid::setWestBorder(T func){
	int r = 0;
	for (int s = 1; s < getNumberOfPointsS() - 1; ++s) {
		operator()(r, s) = func(getX(r, s), getY(r, s));
	}
}

template <typename T>
void Grid::setEastBorder(T func){
	int r = getNumberOfPointsR() - 1;
	for (int s = 1; s < getNumberOfPointsS() - 1; ++s) {
		operator()(r, s) = func(getX(r, s), getY(r, s));
	}
}

#endif
